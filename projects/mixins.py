from django.contrib.postgres.search import SearchVector, SearchRank
from django.db.models import Q, OuterRef
from django.db.models.functions import Coalesce
from django.db.models import Value, CharField

from projects.models import  Wtg


class RelatedModelSearchMixin:
    search_param = 'search'
    related_models = []
    related_fields = []
    model_fields = []

    def get_queryset(self):
        queryset = super().get_queryset()
        search_term = self.request.GET.get(self.search_param, '')
        if not search_term:
            return queryset

        annotations = {}
        search_vectors = []
        search_fields = []
        for model, fields in zip(self.related_models, self.related_fields):
            model_name = model.__name__.lower()
            search_vectors.extend([f'{model_name}__{f}' for f in search_fields])
        search_fields.extend(self.model_fields)
        search_vectors.extend(search_fields)
        subquery = Wtg.objects.filter(
            project_id=OuterRef('id')
        ).order_by().values_list('wtg_number', flat=True)[:1]
        annotations['search'] = SearchVector(
            Coalesce(subquery, Value(''), output_field=CharField()),
            *search_vectors,
        )
        queryset = queryset.annotate(**annotations)

        q_objects = Q(search=search_term)
        for f in self.model_fields:
            q_objects |= Q(**{f'{f}__icontains': search_term})
        for model, fields in zip(self.related_models, self.related_fields):
            model_name = model.__name__.lower()
            for field in fields:
                if field in [f.name for f in model._meta.get_fields()]:
                    q_objects |= Q(**{f'{model_name}__{field}__icontains': search_term})

        print("q objecfts", q_objects)

        queryset = queryset.filter(q_objects).distinct()

        queryset = queryset.annotate(rank=SearchRank('search', search_term)).order_by('-rank')

        return queryset
