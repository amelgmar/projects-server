from datetime import datetime
from rest_framework import serializers
from .models import Project


class ProjectSerializer(serializers.ModelSerializer):
    wtg_numbers = serializers.SerializerMethodField("get_wtg_numbers", read_only=True)
    total_kw = serializers.SerializerMethodField("get_total_kw", read_only=True)
    months_acquired = serializers.SerializerMethodField("get_months_acquired", read_only=True)

    def get_wtg_numbers(self, project):
        return ",".join([wtg.wtg_number for wtg in project.wtg.all()])

    def get_total_kw(self, project):
        return sum([wtg.wtg_kw for wtg in project.wtg.all()])

    def get_months_acquired(self, project):
        if project.acquisition_date:
            return (datetime.now().year - project.acquisition_date.year) * 12 + \
                   datetime.now().month - project.acquisition_date.month

        return 0

    class Meta:
        model = Project
        fields = ['id', 'project_name', 'project_number', 'acquisition_date', 'number_3l_code',
                  'project_deal_type_id', 'project_group_id', 'project_status_id',
                  'wtg_numbers', 'total_kw', 'months_acquired']
