from django.test import TestCase
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APIClient

from .models import Project, Wtg


class AuthorSearchTestCase(TestCase):
    def setUp(self):
        self.client = APIClient()

        self.project1 = Project.objects.create(project_name='Project 1', acquisition_date='2023-03-18')
        self.project2 = Project.objects.create(project_name='Project 2', acquisition_date='2023-02-01')
        Wtg.objects.create(
            wtg_number='Num1234', wtg_kw=10,
            project=self.project1
        )
        Wtg.objects.create(
            wtg_number='Num7890', wtg_kw=15,
            project=self.project1
        )
        Wtg.objects.create(
            wtg_number='Num5467', wtg_kw=20,
            project=self.project2
        )

    def test_serializer_custom_fields(self):
        response = self.client.get(reverse('projects-list'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data[0]['wtg_numbers'], 'Num1234,Num7890')
        self.assertEqual(response.data[0]['total_kw'], 25)

    def test_search_by_project_name(self):
        response = self.client.get(reverse('projects-list'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        print(response.data)
        self.assertEqual(len(response.data), 2)
        self.assertEqual(response.data[0]['project_name'], 'Project 1')

    def test_search_by_wtg_number(self):
        response = self.client.get(reverse('projects-list'), {'search': 'num12'})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)
        self.assertEqual(response.data[0]['project_name'], 'Project 1')

    def test_filter_by_project_name(self):
        response = self.client.get(reverse('projects-list'), {'project_name': 'Project 2'})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)
        self.assertEqual(response.data[0]['project_name'], 'Project 2')

    def test_sort_by_project_name(self):
        response = self.client.get(reverse('projects-list'), {'ordering': 'project_name'})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 2)
        self.assertEqual(response.data[0]['project_name'], 'Project 1')
        self.assertEqual(response.data[1]['project_name'], 'Project 2')

    def test_pagination(self):
        response = self.client.get(reverse('projects-list'), {'page': 1, 'page_size': 1})
        print(response.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["count"], 2)
        self.assertEqual(response.data["results"][0]['project_name'], 'Project 1')
        response = self.client.get(reverse('projects-list'), {'page': 2, 'page_size': 1})
        print(response.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["count"], 2)
        self.assertEqual(response.data["results"][0]['project_name'], 'Project 2')
