from django.contrib.postgres.search import SearchVectorField, SearchVector
from django.db import models


class Project(models.Model):
    PROJECT_DEAL_TYPE_CHOICES = (
        ('Share', 'Share'),
        ('Asset', 'Asset'),
    )
    PROJECT_GROUP_ID_CHOICES = (
        ('RW 1', 'RW 1'),
        ('QE 4', 'QE 4'),
    )
    PROJECT_STATUS_ID_CHOICES = (
        ('1 Operating', '1 Operating'),
        ('2 DD', '2 DD'),
    )
    project_name = models.CharField(max_length=100, null=True, blank=True)
    project_number = models.CharField(max_length=100, null=True, blank=True)
    acquisition_date = models.DateField(null=True)
    number_3l_code = models.CharField(max_length=4, null=True, blank=True)
    project_deal_type_id = models.CharField(max_length=5, choices=PROJECT_DEAL_TYPE_CHOICES)
    project_group_id = models.CharField(max_length=4, choices=PROJECT_GROUP_ID_CHOICES)
    project_status_id = models.CharField(max_length=11, choices=PROJECT_STATUS_ID_CHOICES)

    def __str__(self):
        return self.project_name


class Wtg(models.Model):
    wtg_number = models.CharField(max_length=100, primary_key=True)
    wtg_kw = models.IntegerField(default=0)
    project = models.ForeignKey(Project, related_name='wtg', on_delete=models.CASCADE)

    def __str__(self):
        return self.wtg_number
