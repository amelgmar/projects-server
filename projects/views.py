from rest_framework import viewsets, filters, status
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.response import Response

from .models import Project, Wtg
from .serialisers import ProjectSerializer
from .mixins import RelatedModelSearchMixin
from .pagination import CustomPagination


class ProjectViewSet(RelatedModelSearchMixin, viewsets.ModelViewSet):
    queryset = Project.objects.all()
    serializer_class = ProjectSerializer
    related_models = [Wtg]
    related_fields = [['wtg_number']]
    model_fields = ['project_name']
    filter_backends = [filters.OrderingFilter, DjangoFilterBackend]
    ordering_fields = ['project_name', 'project_number', 'acquisition_date', 'number_3l_code',
                       'project_deal_type_id', 'project_group_id', 'project_status_id']
    filterset_fields = ['project_name', 'project_number', 'acquisition_date',
                        'number_3l_code', 'project_deal_type_id', 'project_group_id', 'project_status_id']
    pagination_class = CustomPagination

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        return Response(serializer.data, status=status.HTTP_201_CREATED)

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        print("serialiser data", serializer.validated_data)
        self.perform_update(serializer)

        return Response(serializer.data)

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        self.perform_destroy(instance)
        return Response(status=status.HTTP_204_NO_CONTENT)
