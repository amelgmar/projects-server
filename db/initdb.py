import csv
import os
from datetime import datetime

import psycopg2
from decouple import config


def migrate_database():
    os.system('python manage.py makemigrations')
    os.system('python manage.py makemigrations projects')
    os.system('python manage.py migrate')


def restore_csv_data():
    conn = psycopg2.connect(
        host=config("DATABASE_HOST"),
        user=config("DATABASE_USER"),
        password=config("DATABASE_PASSWORD"),
        dbname=config("DATABASE_NAME")
    )
    conn.set_session(autocommit=True)

    with open('db/projects.csv') as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            cur = conn.cursor()
            cur.execute("""
                INSERT INTO projects_project (id, project_name, project_number,acquisition_date, number_3l_code, 
                project_deal_type_id, project_group_id, project_status_id)
                VALUES (%s, %s, %s, %s, %s, %s, %s, %s)
            """, (row['id'], row['project_name'], row['project_number'], datetime.strptime(row['acquisition_date'],
                                                                                           "%d/%m/%Y").date().
                  strftime("%Y-%m-%d") if row['acquisition_date'] != "NULL" else None,
                  row['number_3l_code'], row['project_deal_type_id'], row['project_group_id'], row['project_status_id']))
            cur.close()

    with open('db/wtg.csv') as csvfile1:
        reader1 = csv.DictReader(csvfile1)
        for row1 in reader1:
            cur1 = conn.cursor()
            cur1.execute("""
                INSERT INTO projects_wtg (wtg_number, wtg_kw, project_id)
                VALUES (%s, %s, %s)
            """, (row1['WTG_number'], row1['kW'], row1['project_id']))
            cur1.close()

    conn.close()


if __name__ == '__main__':
    migrate_database()
    restore_csv_data()
