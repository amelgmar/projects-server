#!/bin/bash
# IMPORTANT: Used only in development environment

# Simulating deployment steps (i.e like Terraform in non-local env):-
# Create kelvyn database if doesn't exist
echo "user" $POSTGRES_USER
if psql -U $POSTGRES_USER -lqt | cut -d \| -f 1 | grep -qw "$POSTGRES_DB"; then
  printf "\"$POSTGRES_DB\" already exists\n";
else
  #export PGPASSWORD=$POSTGRES_PASSWORD
  psql -v ON_ERROR_STOP=1 --username postgres --dbname postgres <<-EOSQL
CREATE USER $POSTGRES_USER WITH PASSWORD $POSTGRES_PASSWORD;
CREATE DATABASE $POSTGRES_DB;
GRANT ALL PRIVILEGES ON DATABASE $POSTGRES_DB TO $POSTGRES_USER;
EOSQL
  echo "Created database $POSTGRES_DB owned by user $POSTGRES_USER with password $POSTGRES_PASSWORD"
fi
